/// @file 
/// @brief Main application file

#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @name Define fixed size integers within file
/// @{
#ifndef uint8_t
	/// 8-bit integer definition
	typedef unsigned char uint8_t;
#endif
#ifndef uint16_t
	/// 16-bit integer definition
	typedef unsigned short int uint16_t;
#endif
#ifndef uint32_t
	/// 32-bit integer definition
	typedef unsigned int uint32_t;
#endif
#ifndef uint64_t
	/// 64-bit integer definition
	typedef unsigned long int uint64_t;
#endif
/// @}

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
	fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
/// Structure containing COFF file header data
struct PEHeader
{
	/// Type of target machine
	uint16_t Machine;
	/// Size of the section table
	uint16_t NumberOfSections;
	/// When the file was created
	uint32_t TimeDateStamp;
	/// Offset to COFF symbol table
	uint32_t PointerToSymbolTable;
	/// Size of the COFF symbol table
	uint32_t NumberOfSymbols;
	/// Size of the optional header
	uint16_t SizeOfOptionalHeader;
	/// Flags that indicate file attributes
	uint16_t Characteristics;
}
#ifdef __GNUC__
	__attribute__((packed))
#endif
;
#ifdef _MSC_VER
	#pragma pack(pop)
#endif


#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
/// Structure containing optional header data
struct OptionalHeader
{
	/// Indicates the PE-file format
	uint16_t Magic;
	/// The linker major version number.
	uint8_t MajorLinkerVersion;
	/// The linker minor version number.
	uint8_t MinorLinkerVersion;
	/// Size of code (text) section
	uint32_t SizeOfCode;
	/// Size of the initialized data section
	uint32_t SizeOfInitializedData;
	/// Size of the uninitialized data section
	uint32_t SizeOfUninitializedData;
	/// Offset to entry point
	uint32_t AddressOfEntryPoint;
	/// Offset to code section in memory
	uint32_t BaseOfCode;
	/// Offset to image in memory. Present only in PE32
	uint32_t BaseOfData;
	/// Preferred to the first byte of image
	uint64_t ImageBase;
	/// Section alignment in memory
	uint32_t SectionAlignment;
	/// Raw data alignment in memory
	uint32_t FileAlignment;
	/// Major version of required operationg system
	uint16_t MajorOperatingSystemVersion;
	/// Minor version of required operationg system
	uint16_t MinorOperatingSystemVersion;
	/// Major version of image
	uint16_t MajorImageVersion;
	/// Minor version of image
	uint16_t MinorImageVersion;
	/// Major version of subsystem
	uint16_t MajorSubsystemVersion;
	/// Minor version of subsystem
	uint16_t MinorSubsystemVersion;
	/// Must be zero
	uint32_t Win32VersionValue;
	/// Size of image in memory
	uint32_t SizeOfImage;
	/// Size of MS-DOS stub, PE header and section headers in memory
	uint32_t SizeOfHeaders;
	/// Image file checksum
	uint32_t CheckSum;
	/// Subsystem required to run image
	uint16_t Subsystem;
	/// DLL characteristics
	uint16_t DllCharacteristics;
	/// Size reserved on stack
	uint64_t SizeOfStackReserve;
	/// Size of stack to commit
	uint64_t SizeOfStackCommit;
	/// Size reserved on local heap
	uint64_t SizeOfHeapReserve;
	/// Size of local heap to commit
	uint64_t SizeOfHeapCommit;
	/// Must be zero
	uint32_t LoaderFlags;
	/// Number of data-directory entries
	uint32_t NumberOfRvaAndSizes;
}
#ifdef __GNUC__
	__attribute__((packed))
#endif
;
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
/// Structure containing section header data
struct SectionHeader
{
	/// Section name
	uint8_t *Name;
	/// Size of section when loaded into memory
	uint32_t VirtualSize;
	/// Offset to section in memory
	uint32_t VirtualAddress;
	/// Size of section raw data
	uint32_t SizeOfRawData;
	/// Offset to section raw data
	uint32_t PointerToRawData;
	/// Offset to section relocations
	uint32_t PointerToRelocations;
	/// Offset to section linenumbers
	uint32_t PointerToLinenumbers;
	/// Size of section relocations
	uint16_t NumberOfRelocations;
	/// Size of section linenumbers
	uint16_t NumberOfLinenumbers;
	/// Flags that indicate section attributes
	uint32_t Characteristics;
}
#ifdef __GNUC__
	__attribute__((packed))
#endif
;
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
/// Structure containing PE file data
struct PEFile
{
	/// @name Offsets within file
	///@{

	/// Offset to a file magic
	uint32_t magic_offset;
	/// Offset to a main PE header
	uint32_t header_offset;
	/// Offset to an optional header
	uint32_t optional_header_offset;
	/// Offset to a section table
	uint32_t section_header_offset;
	///@}

	/// @name File headers
	///@{

	/// File magic
	uint32_t Magic;
	/// Main header
	struct PEHeader header;
	/// Optional header
	struct OptionalHeader optional_header;
	/// Array of section headers with the size of header.number_of_sections
	struct SectionHeader *section_headers;
	///@}
}
#ifdef __GNUC__
	__attribute__((packed))
#endif
;
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
	/// @details Steps performed:
	///@{
	(void) argc; (void) argv; // supress 'unused parameters' warning
	/// - Check the amount of arguments
	if (argc != 4){
		usage(stdout);
		return 0;
	}
	/// - Open and check the input and output files
	FILE * pf = fopen(argv[1], "rb");
	FILE * output = fopen(argv[3], "w+");
	if (pf == NULL || output == NULL)
	{
		usage(stdout);
		return 0;
	}
	/// - Read the name of target section
	uint8_t *sect = malloc(9);
	uint8_t sect_len = 0;
	uint8_t x = 0;
	while (argv[2][sect_len] && sect_len < 9)
	{
		sect[sect_len] = argv[2][sect_len];
		sect_len++;
	}
	/// - Reading and counting PE file offsets
	struct PEFile pefile;
	fseek(pf, 0x3c, 0);
	fread(&pefile.magic_offset, sizeof(pefile.magic_offset), 1, pf);
	fseek(pf, pefile.magic_offset, 0);
	pefile.header_offset = pefile.magic_offset + sizeof(pefile.magic_offset);
	pefile.optional_header_offset = pefile.header_offset + sizeof(pefile.header);
	/// - Reading PE header
	fread(&pefile.Magic, sizeof(pefile.Magic), 1, pf);
	fread(&pefile.header.Machine, sizeof(pefile.header.Machine), 1, pf);
	fread(&pefile.header.NumberOfSections, sizeof(pefile.header.NumberOfSections), 1, pf);
	fread(&pefile.header.TimeDateStamp, sizeof(pefile.header.TimeDateStamp), 1, pf);
	fread(&pefile.header.PointerToSymbolTable, sizeof(pefile.header.PointerToSymbolTable), 1, pf);
	fread(&pefile.header.NumberOfSymbols, sizeof(pefile.header.NumberOfSymbols), 1, pf);
	fread(&pefile.header.SizeOfOptionalHeader, sizeof(pefile.header.SizeOfOptionalHeader), 1, pf);
	fread(&pefile.header.Characteristics, sizeof(pefile.header.Characteristics), 1, pf);
 
	pefile.section_header_offset = pefile.optional_header_offset+pefile.header.SizeOfOptionalHeader;
	/// - Reading optional header
	if (pefile.header.SizeOfOptionalHeader > 19)
	{
		fread(&pefile.optional_header.Magic, sizeof(pefile.optional_header.Magic), 1, pf);
		fread(&pefile.optional_header.MajorLinkerVersion, sizeof(pefile.optional_header.MajorLinkerVersion), 1, pf);
		fread(&pefile.optional_header.MinorLinkerVersion, sizeof(pefile.optional_header.MinorLinkerVersion), 1, pf);
		fread(&pefile.optional_header.SizeOfCode, sizeof(pefile.optional_header.SizeOfCode), 1, pf);
		fread(&pefile.optional_header.SizeOfInitializedData, sizeof(pefile.optional_header.SizeOfInitializedData), 1, pf);
		fread(&pefile.optional_header.SizeOfUninitializedData, sizeof(pefile.optional_header.SizeOfUninitializedData), 1, pf);
		fread(&pefile.optional_header.AddressOfEntryPoint, sizeof(pefile.optional_header.AddressOfEntryPoint), 1, pf);
		fread(&pefile.optional_header.BaseOfCode, sizeof(pefile.optional_header.BaseOfCode), 1, pf);
	}
	if (pefile.header.SizeOfOptionalHeader > 27)
	{
		if (pefile.optional_header.Magic == 0x20b)
		{
			fread(&pefile.optional_header.ImageBase, sizeof(pefile.optional_header.ImageBase), 1, pf);
		}
		else
		{
			fread(&pefile.optional_header.BaseOfData, sizeof(pefile.optional_header.BaseOfData), 1, pf);
			pefile.optional_header.ImageBase = 0;
			fread(&pefile.optional_header.ImageBase, sizeof(uint32_t), 1, pf);
		}

		fread(&pefile.optional_header.SectionAlignment, sizeof(pefile.optional_header.SectionAlignment), 1, pf);
		fread(&pefile.optional_header.FileAlignment, sizeof(pefile.optional_header.FileAlignment), 1, pf);
		fread(&pefile.optional_header.MajorOperatingSystemVersion, sizeof(pefile.optional_header.MajorOperatingSystemVersion), 1, pf);
		fread(&pefile.optional_header.MinorOperatingSystemVersion, sizeof(pefile.optional_header.MinorOperatingSystemVersion), 1, pf);
		fread(&pefile.optional_header.MajorImageVersion, sizeof(pefile.optional_header.MajorImageVersion), 1, pf);
		fread(&pefile.optional_header.MinorImageVersion, sizeof(pefile.optional_header.MinorImageVersion), 1, pf);
		fread(&pefile.optional_header.MajorSubsystemVersion, sizeof(pefile.optional_header.MajorSubsystemVersion), 1, pf);
		fread(&pefile.optional_header.MinorSubsystemVersion, sizeof(pefile.optional_header.MinorSubsystemVersion), 1, pf);
		fread(&pefile.optional_header.Win32VersionValue, sizeof(pefile.optional_header.Win32VersionValue), 1, pf);
		fread(&pefile.optional_header.SizeOfImage, sizeof(pefile.optional_header.SizeOfImage), 1, pf);
		fread(&pefile.optional_header.SizeOfHeaders, sizeof(pefile.optional_header.SizeOfHeaders), 1, pf);
		fread(&pefile.optional_header.CheckSum, sizeof(pefile.optional_header.CheckSum), 1, pf);
		fread(&pefile.optional_header.Subsystem, sizeof(pefile.optional_header.Subsystem), 1, pf);
		fread(&pefile.optional_header.DllCharacteristics, sizeof(pefile.optional_header.DllCharacteristics), 1, pf);

		if (pefile.optional_header.Magic == 0x20b)
		{
			fread(&pefile.optional_header.SizeOfStackReserve, sizeof(pefile.optional_header.SizeOfStackReserve), 1, pf);
			fread(&pefile.optional_header.SizeOfStackCommit, sizeof(pefile.optional_header.SizeOfStackCommit), 1, pf);
			fread(&pefile.optional_header.SizeOfHeapReserve, sizeof(pefile.optional_header.SizeOfHeapReserve), 1, pf);
			fread(&pefile.optional_header.SizeOfHeapCommit, sizeof(pefile.optional_header.SizeOfHeapCommit), 1, pf);
		}
		else
		{
			pefile.optional_header.SizeOfStackReserve = 0;
			fread(&pefile.optional_header.SizeOfStackReserve, sizeof(uint32_t), 1, pf);
			pefile.optional_header.SizeOfStackCommit = 0;
			fread(&pefile.optional_header.SizeOfStackCommit, sizeof(uint32_t), 1, pf);
			pefile.optional_header.SizeOfHeapReserve = 0;
			fread(&pefile.optional_header.SizeOfHeapReserve, sizeof(uint32_t), 1, pf);
			pefile.optional_header.SizeOfHeapCommit = 0;
			fread(&pefile.optional_header.SizeOfHeapCommit, sizeof(uint32_t), 1, pf);
		}

		fread(&pefile.optional_header.LoaderFlags, sizeof(pefile.optional_header.LoaderFlags), 1, pf);
		fread(&pefile.optional_header.NumberOfRvaAndSizes, sizeof(pefile.optional_header.NumberOfRvaAndSizes), 1, pf);
	}

	/// - Reading all sections in a loop
	fseek(pf, pefile.section_header_offset, 0);
	pefile.section_headers = calloc(sizeof(struct SectionHeader), pefile.header.NumberOfSections);
	for (uint16_t i=0; i<pefile.header.NumberOfSections; i++)
	{
		pefile.section_headers[i].Name = malloc(9);
		fread(pefile.section_headers[i].Name, 8, 1, pf);
		pefile.section_headers[i].Name[8] = '\0';
		fread(&pefile.section_headers[i].VirtualSize, sizeof(pefile.section_headers[0].VirtualSize), 1, pf);
		fread(&pefile.section_headers[i].VirtualAddress, sizeof(pefile.section_headers[0].VirtualAddress), 1, pf);
		fread(&pefile.section_headers[i].SizeOfRawData, sizeof(pefile.section_headers[0].SizeOfRawData), 1, pf);
		fread(&pefile.section_headers[i].PointerToRawData, sizeof(pefile.section_headers[0].PointerToRawData), 1, pf);
		fread(&pefile.section_headers[i].PointerToRelocations, sizeof(pefile.section_headers[0].PointerToRelocations), 1, pf);
		fread(&pefile.section_headers[i].PointerToLinenumbers, sizeof(pefile.section_headers[0].PointerToLinenumbers), 1, pf);
		fread(&pefile.section_headers[i].NumberOfRelocations, sizeof(pefile.section_headers[0].NumberOfRelocations), 1, pf);
		fread(&pefile.section_headers[i].NumberOfLinenumbers, sizeof(pefile.section_headers[0].NumberOfLinenumbers), 1, pf);
		fread(&pefile.section_headers[i].Characteristics, sizeof(pefile.section_headers[0].Characteristics), 1, pf);
		///	- Checking if it is target
		for (x=0; x<sect_len; x++)
		{
			if (pefile.section_headers[i].Name[x] != sect[x])
			{
				break;
			}
		}
		if (x == sect_len)
		{
			///	- Writing raw section data to output file and exiting the section reading loop
			fseek(pf, pefile.section_headers[i].PointerToRawData, 0);
			uint8_t *buf = malloc(pefile.section_headers[i].SizeOfRawData+1);
			fread(buf, pefile.section_headers[i].SizeOfRawData, 1, pf);
			buf[pefile.section_headers[i].SizeOfRawData]='\0';
			fwrite(buf, pefile.section_headers[i].SizeOfRawData, 1, output);
			free(buf);
			break;
			
		}
	}
	/// - Freeing reserved space
	for (uint16_t i=0; i<pefile.header.NumberOfSections; i++)
	{
		free(pefile.section_headers[i].Name);
	}
	free(pefile.section_headers);
	free(sect);
	/// - Closing input and output file
	fclose(pf);
	fclose(output);
	return 0;
	/// @}
}
